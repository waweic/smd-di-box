# Simple phantom-powered active DI-Box in SMD

*Disclaimer: This was shamelessly stolen from [this webpage](https://www.parasitstudio.se/stripboard-layouts/active-direct-injection-box-di) without the authors permission (Yes, I will ask, later...) On the other hand, they themselves got the schematic from [that webpage](https://sound-au.com/project35.htm), which I sadly discovered later on, after sketching the schematic from the stripboard-layout*

I have just ordered the PCB and thus not yet tested the device. **It may very well not work!** This is just something I made for training my general skills in electronics and just the second PCB I design on my own and my first ever project which utilizes SMD components. There may be (and probably are) major flaws in its design. If you notice any, I would be glad to read your advice. 
I ordered the PCB at JLCPCB, including their SMT service. The connectors and the Z-Diode are from LCSC.

Panelizing can be done in KiCAD, at least I did it this way. 

If you have any questions, feel free to ask me here or at [Mastodon](https://chaos.social/@waweic/)

![3D-model of the PCB](Images/DI-Box-SMD-3d.png)
